# Simple Config Version Checker. SmashPuppet(NA)2018
# https://koreanrandom.com/forum/topic/43273-simple-config-version-check-in-the-hangargarage/
import BigWorld
from Account import PlayerAccount
from xfw import *
from xfw_actionscript.python import *
from gui.Scaleform.daapi.view.lobby.hangar.Hangar import Hangar
from xvm_main.python.logger import *
from xvm_main.python import config
from debug_utils import LOG_CURRENT_EXCEPTION
import urllib
from distutils.version import StrictVersion
import re
from adisp import process
import Keys
import game
from adisp import process, async
from helpers import dependency
from skeletons.gui.game_control import IBrowserController
from gui.Scaleform.daapi.view.meta.BrowserViewMeta import BrowserViewMeta
from WebBrowser import WebBrowser
import random

autoReloadConfig = False
isBattle = False
browserCtrl = dependency.instance(IBrowserController)
player = BigWorld.player()

def readConfig():
    global autoReloadConfig, versionConfig, cName, remoteURL, updateURL, search, updateConfig, currentConfig, updateColor, currentColor, notAvailable, versionConflict, conflictInstruction, updateInstruction
    autoReloadConfig = config.get('autoReloadConfig')
    versionConfig = config.get('configVersionCheck', {'enabled': True})
    cName = config.get('configVersionCheck/settings/configName')
    remoteURL = config.get('configVersionCheck/links/remoteLink')
    updateURL = config.get('configVersionCheck/links/updateLink')
    search = config.get('configVersionCheck/settings/version')
    updateConfig = config.get('configVersionCheck/settings/updateConfig')
    currentConfig = config.get('configVersionCheck/settings/currentConfig')
    updateColor = config.get('configVersionCheck/settings/updateColor')
    currentColor = config.get('configVersionCheck/settings/currentColor')
    notAvailable = config.get('configVersionCheck/settings/notAvailable')
    updateInstruction = config.get('configVersionCheck/settings/updateInstruction')
    versionConflict = config.get('configVersionCheck/settings/versionConflict')
    conflictInstruction = config.get('configVersionCheck/settings/conflictInstruction')

readConfig()


@registerEvent(PlayerAccount, 'onArenaCreated')
def onArenaCreated(self):
    global isBattle
    isBattle = True


@registerEvent(Hangar, '_Hangar__updateParams')
def Hangar__updateParams(self):
    global isBattle
    isBattle = False


@xvm.export('xvm.checkVersion', deterministic=False)
def xvm_checkVersion():
    global lines
    lines=open('G:\cit.txt').read().splitlines()
    return random.choice(lines)


@xvm.export('xvm.mesaj', deterministic=False)
def xvm_mesaj():
   if isBattle: 
    message="hi all"
    player.guiSessionProvider.shared.chatCommands.proto.arenaChat.broadcast(message, 0)
#    global lines
#    lines=open('G:\cit.txt').read().splitlines()
#    return random.choice(lines)
# @process
# def openBrowserWindow(url, title):
    # width = 1200
    # height = 650
    # browserSize = [width, height]
    # yield browserCtrl.load(url=url, title=title, showActionBtn=True, browserSize=browserSize, showCloseBtn=True)

# @registerEvent(game, 'handleKeyEvent')
# def handleKeyEvent(event):
    # if autoReloadConfig:
        # readConfig()
    # if versionConfig['enabled']:
        # if not isBattle:
            # if cmp is not 0:
                # isDown, key, mods, isRepeat = game.convertKeyEvent(event)
                # if key == Keys.KEY_F8 and isDown:
                    # openBrowserWindow(updateURL, (cName + '  ' + updateURL))
    # return None